/* 
 * Name: TestContactListSync 
 * Description: Test
 *                  - Contact Outlook sync process 
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
@isTest (seeAllData = false)
private class TestContactListSync 
{
   private static Contact[] prepareTestData() 
   {
      Contact[] contactlist;
      
      Account acct = new Account( Name = 'Acct' );
      insert acct;
      Account[] testacct = [select Id from Account where CreatedDate = TODAY];
      System.assertEquals(1, testacct.size());

      Contact contact = new Contact( FirstName = 'Test', LastName = 'Client', AccountId = acct.Id);
      insert contact;
      Contact contact2 = new Contact( FirstName = 'Test', LastName = 'User', AccountId = acct.Id);
      insert contact2;
      contactlist = [select Id from contact where CreatedDate = TODAY];
        System.assertEquals(2, contactlist.size());

      T1C_Base__Employee__c newEmp = new T1C_Base__Employee__c(T1C_Base__First_Name__c = 'TestEmp', T1C_Base__Last_Name__c = 'LN', T1C_Base__User__c = UserInfo.getUserId());
      insert newEmp;
      T1C_Base__Employee__c[] testEmp = [select Id from T1C_Base__Employee__c where CreatedDate = TODAY];
      System.assertEquals(1, testEmp.size());

      return contactlist;
   }

   public static testMethod void test_m00() 
   {
      Contact[] contactlist = prepareTestData();
      
      Test.startTest();

      //sync
      Map<String,String> data = new Map<String,String>();
        data.put('contactsIds', contactlist[0].Id + ',' + contactlist[1].Id);
      data.put('sync', 'sync');
      ContactListSyncController m = new ContactListSyncController();
      String result = ContactListSyncController.syncContactLists(JSON.serialize(data));

      //checking
      T1C_Base__Ace_List__c[] aList = [select Id from T1C_Base__Ace_List__c where Outlook_Sync__c = true and T1C_Base__Content_Type__c = 'Contact' and OwnerId = :UserInfo.getUserId()];
      system.assertEquals(aList.size(), 1);
      T1C_Base__Ace_List_Entry__c[] anEntryList = [select Id from T1C_Base__Ace_List_Entry__c where CreatedDate = TODAY];
      system.assertEquals(anEntryList.size(), 2);
      Contact[] contlist = [select Id from contact where CreatedDate = TODAY];
      system.assertEquals(contlist.size(), 2);

      //unsync
      data = new Map<String,String>();
        data.put('contactsIds', contactlist[0].Id);
      data.put('sync', 'un-sync');
      result = ContactListSyncController.syncContactLists(JSON.serialize(data));

      //checking
      aList = [select Id from T1C_Base__Ace_List__c where T1C_Base__Content_Type__c = 'Contact' and OwnerId = :UserInfo.getUserId()];
      system.assertEquals(aList.size(), 1);
      anEntryList = [select Id from T1C_Base__Ace_List_Entry__c where CreatedDate = TODAY];
      system.assertEquals(anEntryList.size(), 1);
      
      Test.stopTest();
   }   
}