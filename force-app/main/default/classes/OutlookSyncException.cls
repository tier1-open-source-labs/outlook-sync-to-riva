/*
 * Name:        OutlookSyncException 
 *
 * Description: Exception Handler for Outlook sync Ace List
 *             
 * Notes: 
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
 public class OutlookSyncException extends Exception {}