/*
 * Name:        ContactEmployeeOutlookSyncController
 *
 * Description: Controller for ContactEmployeeOutlookSyncInfo java script
 *             
 * Notes: 
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
global class ContactEmployeeOutlookSyncController
{
    private static String CONTACT_OUTLOOK_SYNC = 'ACE.Extensions.ContactEmployeeOutlookSync';

    WebService static list<T1C_Base__Ace_List_Entry__c> contactEmployeeInfo(String recordId)
    {
        //Map<String,String> dataDetail = ( Map<String,String>)JSON.deserialize(data, map<string,string>.class);
        //String recordId = dataDetail.get('recordId');
        Contact[] contList;

        system.debug('recordId>>>>>>' + recordId);

        //if record Id is a contact Id
        if (recordId.startsWith('003'))
        {
            //find out the record passed as contact is a synced contact
            contList = [select Id, Name from Contact where Id = :recordId and Outlook_Sync__c = true];
        }
        //if record Id is an account Id
        else if (recordId.startsWith('001'))
        {
            //find out the record passed as contact is a synced contact
            contList = [select Id, Name from Contact where AccountId = :recordId and Outlook_Sync__c = true];
        }

        system.debug('contList>>>>>>' + contList);

        if (contList != null && !contList.isEmpty())
        {
            T1C_FR.Feature contactEmployeeOutlookSyncFeature = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), CONTACT_OUTLOOK_SYNC);

            String query = 'Select ';
    
            for (T1C_FR.Feature columnFeature : contactEmployeeOutlookSyncFeature.SubFeatures)
            {
                query += AFRUtil.getAttributeValue(columnFeature, 'DataField') + ', ';
            }

            query += ' Id from T1C_Base__Ace_List_Entry__c where T1C_Base__Contact__c IN :contList';

            system.debug ('query>>>>>>' + query);

            list<T1C_Base__Ace_List_Entry__c> entries = database.query(query);

            system.debug ('entries>>>>>>' + entries);
    
            return entries;
        }
        return null;
    }

    WebService static list<Column> getColumns()
    {
        T1C_FR.Feature contactEmployeeOutlookSyncFeature = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), CONTACT_OUTLOOK_SYNC);

        list<Column> cols = new list<Column>();

        for (T1C_FR.Feature columnFeature : contactEmployeeOutlookSyncFeature.SubFeatures)
        {
            cols.add(new Column(columnFeature));
        }

        return cols;
    }

    global class Column
    {
        WebService String label;
        WebService String dataField;
        WebService String fieldType;
        WebService Integer order;
        WebService Boolean visible;

        global Column(T1C_FR.Feature config)
        {
            system.debug(config.Name);

            this.label = AFRUtil.getAttributeValue(config, 'Label');
            this.dataField = AFRUtil.getAttributeValue(config, 'DataField');
            this.fieldType = AFRUtil.getAttributeValue(config, 'FieldType', 'TEXT');
            this.order = AFRUtil.getIntegerAttributeValue(config, 'Order', -1);
            this.visible = AFRUtil.getBooleanAttributeValue(config, 'Visible', false);
        }
    }

    global class ContactInfo
    {
        WebService list<Column> columnConfigs;
        WebService list<T1C_Base__Ace_List_Entry__c> listEntries;

        global contactInfo(list<T1C_Base__Ace_List_Entry__c> entries, list<Column> configs)
        {
            columnConfigs = configs;
            listEntries = entries;
        }
    }
}