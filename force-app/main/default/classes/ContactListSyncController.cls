/*
 * Name:		ContactListSyncController 
 *
 * Description: Controller for Contact sync java script
 *              - create or update Ace List with Outlook_Sync__c = true for the login user
 *              - if it is sync, create an ace list entry for the contact if it is not existed and update Contact.Outlook_Sync__c = true
 *              - if it is un-sync, delete the ace list entry for the contact if it is not existed and update Contact.Outlook_Sync__c = false
 *			   
 * Notes: 
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
global class ContactListSyncController 
{
	public ContactListSyncController() 
	{
		
	}

	WebService static String syncContactLists(String data)
	{
		String syncedResults;

        Map<String,String> dataDetail = ( Map<String,String>)JSON.deserialize(data, map<string,string>.class);
        String contactsIds = dataDetail.get('contactsIds');
        String syncString = dataDetail.get('sync');

        String[] ContactIdList;

        if (contactsIds == null || contactsIds == '')
        {
        	syncedResults = 'Please select contact(s) before click to ' + syncString;
        }
        else
        {
	        ContactIdList = contactsIds.split(',');
	        
			syncedResults = setUserField(ContactIdList, syncString);
		}

    	return JSON.serialize(syncedResults);        
	}
	
	private static String setUserField(String[] ContactIdList, String syncString)
	{
		//to get my contact list
		T1C_Base__Ace_List__c myContSyncList;

		//get the contacts from my synced list
		set<Id> mySyncedContacts = new set<Id>{};
		//update ace list entry to my list
		T1C_Base__Ace_List_Entry__c[] aceListEntryToUpdate = new T1C_Base__Ace_List_Entry__c[]{};
		//update the contacts's Outlook_Sync__c field for RIVA, they need the last modified field
		//Contact[] contactToUpdate = new Contact[]{};
		//to get the list entry in my list to delete
		set<Id> contactIdsTodelete = new set<Id>{};

		String contSyncedMessage = '';
		String mycontactSyncListName = 'My Outlook Sync List';
		String returnString = '';

		//get logged in Employee
		Id loggedInUserId = UserInfo.getUserId();
		T1C_Base__Employee__c[] employee = [select Id, T1C_Base__User__c from T1C_Base__Employee__c where T1C_Base__User__c = :UserInfo.getUserId()];
		T1C_Base__Employee__c loggedInEmployee = employee[0];

		T1C_Base__Ace_List__c[] myCRMContSyncLists = [select Id, Name, OwnerId, T1C_Base__Content_Type__c from  T1C_Base__Ace_List__c where OwnerId =: loggedInUserId and Outlook_Sync__c = true and T1C_Base__Employee__c = :loggedInEmployee.Id];

		if (myCRMContSyncLists == null || myCRMContSyncLists.size() == 0)
		{
			myContSyncList = new T1C_Base__Ace_List__c(Name = mycontactSyncListName, T1C_Base__Employee__c = loggedInEmployee.Id, OwnerId = loggedInUserId, T1C_Base__Content_Type__c = 'Contact', Outlook_Sync__c = true);
			insert myContSyncList;
		}
		else
		{
			myContSyncList = myCRMContSyncLists[0];
			//blind update my list to change the last modified date so RIVA can query it
			update myContSyncList;

			for (T1C_Base__Ace_List_Entry__c entry : [select Id, T1C_Base__Contact__c from T1C_Base__Ace_List_Entry__c where T1C_Base__Ace_List__c =: myContSyncList.Id])
			{
				mySyncedContacts.add(entry.T1C_Base__Contact__c);
			}
		}

		map<Id, Contact> contMap = new map<Id, Contact>([select Id, Name from Contact where Id IN: ContactIdList]);

		for (Id contId : contMap.keyset())
		{
			Contact cont = contMap.get(contId);

			//if the sync button is clicked
			if (syncString == 'sync')
			{
				//create an ace list entry for the contact if it is not existed
				if (mySyncedContacts.contains(contId) != true)
				{
					contSyncedMessage = contSyncedMessage  == '' ? cont.Name : contSyncedMessage + ', ' + cont.Name;

					aceListEntryToUpdate.add(new T1C_Base__Ace_List_Entry__c(T1C_Base__Ace_List__c = myContSyncList.Id, T1C_Base__Contact__c = contId, Name = contId, T1C_Base__Key__c = myContSyncList.Id+''+contId, T1C_Base__Entry_List_Id__c = contId));
				}
			}
			//if the un-sync button is clicked
			else
			{
				if (mySyncedContacts.contains(contId) == true)
				{
					contSyncedMessage = contSyncedMessage  == '' ? cont.Name : contSyncedMessage + ', ' + cont.Name;

					contactIdsTodelete.add(contId);
				}
			}
		}
		//comple the messages
		if (contSyncedMessage != '')
		{
			if (contSyncedMessage.contains(','))
			{
				contSyncedMessage += ' are added to be '+ syncString + 'ed.';
			}
			else
			{
				contSyncedMessage += ' is added to be '+ syncString + 'ed.';
			}
			//returnString = 'Sorry could not ' + syncString + ' the contact(s) you selected. Please contact your administrator for more details.';
			returnString += contSyncedMessage;
		}

		if (!contactIdsTodelete.isEmpty())
		{
			T1C_Base__Ace_List_Entry__c[] aceListEntryToDelete = [select Id from T1C_Base__Ace_List_Entry__c where T1C_Base__Ace_List__c = :myContSyncList.Id and T1C_Base__Contact__c IN :contactIdsTodelete];
			if (aceListEntryToDelete != null && aceListEntryToDelete.size() > 0)
			{
				delete aceListEntryToDelete;
			}
		}

		/*//update ace list
		if (!contactToUpdate.isEmpty())
		{
			update contactToUpdate;
		}*/
		//update/insert myContSyncList
		if (aceListEntryToUpdate.size() > 0)
		{
			upsert aceListEntryToUpdate;
		}

		return returnString;
	}
}