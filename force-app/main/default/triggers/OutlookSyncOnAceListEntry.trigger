/*
 * Name:        OutlookSyncOnAceListEntry 
 *
 * Description: Handle OutlookSync using Ace List Entry 
 *              Update Contact.Outlook_Sync__c when My Outlook Sync List updated
 *               by drag and drop     
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
trigger OutlookSyncOnAceListEntry on T1C_Base__Ace_List_Entry__c (before insert, before update, before delete) 
{
    //check if the trigger is active
    //if it is not, abort
    if(T1C_Base.ACETriggerCheck.isDisabled('OutlookSyncOnAceListEntry'))
    {
        return;
    }

    list<Id> aceListIds = new list<Id>{};
    list<Id> contactIds = new list<Id>{};
    list<Contact> contactToUpsert = new list<Contact>{};

    map<Id,set<Id>> contactIdToCoverageUserIds = new map<Id,set<Id>>{};

    for (T1C_Base__Ace_List_Entry__c acelistEntry : trigger.isDelete ? trigger.old : trigger.new)
    {
        if (acelistEntry.T1C_Base__Contact__c != null)
        {
            aceListIds.add(acelistEntry.T1C_Base__Ace_List__c);
            contactIds.add(acelistEntry.T1C_Base__Contact__c);
        }
    }

    //get the Synced Contact Ace List map
    map<Id, T1C_Base__Ace_List__c> listMap = new map<Id, T1C_Base__Ace_List__c>([select Id, Name, Outlook_Sync__c, T1C_Base__Content_Type__c, OwnerId from T1C_Base__Ace_List__c where Id IN :aceListIds and T1C_Base__Content_Type__c = 'Contact' and Outlook_Sync__c = true]);

    if (listMap.isEmpty())
    {
        return;
    }

    //get coverage info
    for (T1C_Base__Contact_Coverage__c cc : [select T1C_Base__Contact__c, T1C_Base__Employee__r.T1C_Base__User__c from T1C_Base__Contact_Coverage__c where T1C_Base__Contact__c IN :contactIds and T1C_Base__Employee__r.T1C_Base__User__c != null])
    {
        set<Id> userIds = contactIdToCoverageUserIds.get(cc.T1C_Base__Contact__c);
        if (userIds == null)
        {
            userIds = new set<Id>{};
            contactIdToCoverageUserIds.put(cc.T1C_Base__Contact__c, userIds);
        }
        userIds.add(cc.T1C_Base__Employee__r.T1C_Base__User__c);
    }

    //get all the contacts for the Ace List Entries
    map<Id, Contact> contactMap = new map<Id, Contact>([select Id, Outlook_Sync__c from Contact where Id IN: contactIds]);

    for (T1C_Base__Ace_List_Entry__c acelistEntry : trigger.isDelete ? trigger.old : trigger.new)
    {
        T1C_Base__Ace_List__c aceList = listMap.get(acelistEntry.T1C_Base__Ace_List__c);
        if (aceList != null)
        {
            //checkif the list owner cover this contact
            //if he or she does, the set the field in entry records
            //to display the info in cube
            if (!trigger.isDelete)
            {
	            set<Id> userIds = contactIdToCoverageUserIds.get(acelistEntry.T1C_Base__Contact__c);
	            if (userIds != null && userIds.contains(aceList.OwnerId))
	            {
	            	acelistEntry.CoveredByListOwner__c = true;
	            }
	            else
	            {
	            	acelistEntry.CoveredByListOwner__c = false;
	            }
            }
        }
    }

}