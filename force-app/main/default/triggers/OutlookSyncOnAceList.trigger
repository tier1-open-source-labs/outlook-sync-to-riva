/*
 * Name:        OutlookSyncOnAceList
 *
 * Description: Handle OutlookSync using Ace List 
 *
 * Notes:      
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
trigger OutlookSyncOnAceList on T1C_Base__Ace_List__c (before delete, before update) 
{
    //check if the trigger is active
    //if it is not, abort
    if(T1C_Base.ACETriggerCheck.isDisabled('OutlookSyncOnAceList'))
    {
        return;
    }

    //get the Synced Contact Ace List map
    map<Id, T1C_Base__Ace_List__c> listMap = new map<Id, T1C_Base__Ace_List__c>([select Id, Name, Outlook_Sync__c, T1C_Base__Content_Type__c from T1C_Base__Ace_List__c where Id IN :trigger.old and T1C_Base__Content_Type__c = 'Contact' and Outlook_Sync__c = true]);

    //If a list has been sync-ed to outlook for a particular user, 
    //this list has to remain visible in his/her view, meaning the list cannot be deleted or unshared with this user
    for (T1C_Base__Ace_List__c acelist : trigger.isDelete ? trigger.old : trigger.new)
    {
        T1C_Base__Ace_List__c acelistSynced = listMap.get(acelist.Id);

        if (acelistSynced != null)
        {
            if (trigger.isDelete)
            {
                throw new OutlookSyncException('SYNCEDACELIST: You are not allowed to delete Outlook Sync List. Please contact your administrator for more details.');
            }
            else 
            {
                acelist.T1C_Base__Is_Expired__c = false;
            }

            if (trigger.isUpdate)
            {
                T1C_Base__Ace_List__c old = trigger.oldMap.get(acelist.Id);
                acelist.T1C_Base__Employee__c = old.T1C_Base__Employee__c;
                acelist.OwnerId = old.OwnerId;
            }
        }
    }
}