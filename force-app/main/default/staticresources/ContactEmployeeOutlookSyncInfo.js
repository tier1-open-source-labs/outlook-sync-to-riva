//=============================================================================
/**
 * Name: ContactEmployeeOutlookSyncInfo.js
 * Description: It will be the cube to show the synced contacts information. 
 *              If you select an Account, it will show all the contacts from 
 *				the account that is synced to Outlook. If you select a Contact, 
 *				it will show all the Employees who synced the contacts.
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)	
{
	var _this;
  	var cubeBody = $p('<div id="ContactEmployeeOutlookSyncInfo" class="customCubeContainer"/>');
	var entryGrid;

	function ContactEmployeeOutlookSyncInfo()
	{
		if (window.sforce === null || window.sforce === undefined)
	    {
	      	$("head").append('<script type="text/javascript" src="/soap/ajax/35.0/connection.js" />');
	      	$("head").append('<script type="text/javascript" src="/soap/ajax/35.0/apex.js" />');     
	    }
	    if (window.$lightning === null || window.$lightning === undefined)
	    {   
	       	$("head").append('<script type="text/javascript" src="/lightning/lightning.out.js" />');
	    }		

	    if (!String.prototype.startsWith) 
	    {
	      	String.prototype.startsWith = function(searchString, position) 
	      	{
		        position = position || 0;
		        return this.indexOf(searchString, position) === position;
	     	};
    	} 
	}
		
	ContactEmployeeOutlookSyncInfo.prototype.setData = function(dataProvider)
	{	
		if (this._token != null) 
	    {
			this._token.cancel();
	     	this._token = null;
	    }

	    this._token = new AsyncToken(resultHandler);

		sforce.connection.sessionId = ACE.Salesforce.sessionId;
		
		if(entryGrid == undefined)
		{
			var result = sforce.apex.execute("ContactEmployeeOutlookSyncController", "getColumns", {}, 
			{ 
				onSuccess: function(result)
				{		
					var columns = [];
					
					if(!$.isArray(result))
					{
						result = [result];
					}

					for(var x=0; x<result.length; x++)
					{
						var col = result[x];
						columns.push({id: col.dataField, field: col.dataField, width: '100', name: col.label, sortable: true, fieldType: col.fieldType});
					}

					entryGrid = new ACEDataGrid($(cubeBody) ,{  maxNumberOfRows:10,
						autoHeight: true,
						forceFitColumns: true,
						explicitInitialization: true,
						editable: true,                                                                                    
						headerRowHeight: 40
					});
		
					entryGrid.setColumns(columns);
					entryGrid.grid.init();

					sforce.apex.execute("ContactEmployeeOutlookSyncController", "contactEmployeeInfo", {"recordId" : dataProvider.Id}, 
					{ 
						onSuccess: _token.onSuccess,				
						onFailure: _token.onFailure
					});
				}, 
				onFailure: function(error)
				{			
					showError(error);
				} 
			});
		}
		else
		{
			sforce.apex.execute("ContactEmployeeOutlookSyncController", "contactEmployeeInfo", {"recordId" : dataProvider.Id}, 
			{ 
				onSuccess: _token.onSuccess,				
				onFailure: _token.onFailure
			});	
		}
	}

	ContactEmployeeOutlookSyncInfo.prototype.clear = function()
	{
		//Clear the element data to a blank state
    	//cubeBody.empty();
	}
	
	ContactEmployeeOutlookSyncInfo.prototype.cancel = function()
	{
		//Cancel the current pull transaction
		if (this._token != null)
		{
			this._token.cancel();
			this._token = null;
		}
	}
	
	ContactEmployeeOutlookSyncInfo.prototype.measure = function()
	{
	
	}

	ContactEmployeeOutlookSyncInfo.prototype.createChildren = function(parent)
	{		
		cubeBody.appendTo(parent); 
	}

	ContactEmployeeOutlookSyncInfo.prototype.entryGrid = function()
	{

	}

	function AsyncToken(resultHandler)
	{		
		this._isCancelled = false;
		this._resultHandler = resultHandler;
		
		var _this = this;
		_token = _this;
		
		this.onSuccess = function(result)
		{
			_result = result;
			if (!_this._isCancelled)
			{
				resultHandler(_result);	
			}
		};
		
		this.onFailure = function(message)
		{
			if (!_this._isCancelled)
			{
				ACE.FaultHandlerDialog.show(message);
			}
		};
	}
	
	AsyncToken.prototype.cancel = function()
	{
		this._isCancelled = true;
		_token._isCancelled = true;
	}
	
	function resultHandler(result) 
	{
		if (result == null)
	    {
	      return;
	    }

		if(!$.isArray(result))
		{
			result.listEntries = [result];
		}

		for(var x=0; x<result.length; x++)
		{
			result[x].id = result[x].Id;
		}

		entryGrid.setItems(result);		
	}
		
  	$.extend(true, window,
	{
		"Custom":
		{
			"ContactEmployeeOutlookSyncInfo": ContactEmployeeOutlookSyncInfo
		}
	});
//# sourceURL=ContactEmployeeOutlookSyncInfo.js	
})(jQuery);