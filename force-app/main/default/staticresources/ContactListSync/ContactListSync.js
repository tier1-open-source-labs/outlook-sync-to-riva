//=============================================================================
/**
 * Name: ContactListSync.js
 * Description: 
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)    
{
	//handle fault Message when a synced List is get deleted
	function faultHandler(options)
	{
		var error;

		if (typeof options === "string")
		{
			error = options;
		}
		else if (options.error != null)
		{
			//capture the Exception message thrown from the triggers to display user frindly way
			var errorMeg = options.error.message;
			if (errorMeg.indexOf("SYNCEDACELIST:") !== -1)
			{
				var messageList = errorMeg.split("SYNCEDACELIST:");
				var meg = messageList[1];
				meg = meg.replace("(): []", "");
				error = meg;
			}
		}

		ACE.FaultHandlerDialog.show({title:'Unexpected Error Occured', message : error, error : 'Please review you data and try again'});
	}
	
	//click on sync button
    ACE.CustomEventDispatcher.registerEventParams("SyncContactList", function()
    {
        return {};
    });
    
    ACE.CustomEventDispatcher.on("SyncContactList", function(e, eventData)
    {
        sforce.connection.sessionId = ACE.Salesforce.sessionId;

        var resultString;

        var contactsIds = '';
        var selectedContacts = ALM.modelLocator.mainPaneSelectedItems;
        for(var i = 0; i < selectedContacts.length; i++)
        {
        	contactsIds += ALM.modelLocator.mainPaneSelectedItems[i].data.Id + ',';
        }

        var options =  
        {
                "contactsIds" : contactsIds,
                "sync"        : "sync"
        };
    
	  	var data = {"data": JSON.stringify(options)};

	  	var result = sforce.apex.execute("ContactListSyncController", "syncContactLists", data, 
	    { 
	    	onSuccess: function(result)
	    	{		
	    		resultString = JSON.parse(result);
	    		//if it is already synced, do not show any messge
	    		if (resultString != '')
	    		{
		    		ACEConfirm.show
					(
						resultString,
						"",
						[ "Okay"],
						function(result)
						{
							ALM.ListPane.refresh();
						}
					);
				}
	    	}, 
	    	onFailure: function(error)
	    	{		
	    		faultHandler(error);
	    	} 
	    });

        console.log('hello ' + contactsIds);
        return false;
    },2);

    //click on un-sync button
    ACE.CustomEventDispatcher.registerEventParams("UnSyncContactList", function()
    {
        return {};
    });
    
    ACE.CustomEventDispatcher.on("UnSyncContactList", function(e, eventData)
    {
        sforce.connection.sessionId = ACE.Salesforce.sessionId;

        var resultString;

        var contactsIds = '';
        var selectedContacts = ALM.modelLocator.mainPaneSelectedItems;
        for(var i = 0; i < selectedContacts.length; i++)
        {
        	contactsIds += ALM.modelLocator.mainPaneSelectedItems[i].data.Id + ',';
        }
        
        var options =  
        {
                "contactsIds" : contactsIds,
                "sync"        : "un-sync"
        };
    
	  	var data = {"data": JSON.stringify(options)};

	  	var result = sforce.apex.execute("ContactListSyncController", "syncContactLists", data, 
	    { 
	    	onSuccess: function(result)
	    	{		
	    		resultString = JSON.parse(result);
	    		//if it is already synced, do not show any messge
	    		if (resultString != '')
	    		{
		    		ACEConfirm.show
					(
						resultString,
						"",
						[ "Okay"],
						function(result)
						{
							ALM.ListPane.refresh();
						}
					);
				}
	    	}, 
	    	onFailure: function(error)
	    	{		
	    		faultHandler(error);
	    	} 
	    });

        console.log('hello2 ' + contactsIds);
        return false;
    },2);

    //# sourceURL=ContactListSync.js	
})(jQuery);